﻿using System;
using System.IO;
using System.Reflection;

public class Program
{
    public static int Main(string[] args)
    {
        foreach (var arg in args)
        {
            try
            {
                var path = Path.GetFullPath(arg);
                var assembly = System.Runtime.Loader
                    .AssemblyLoadContext.Default
                    .LoadFromAssemblyPath(path);
                
                Console.Out.WriteLine(assembly.GetName().Version);
                return 0;
            }
            catch (Exception exception)
            {
                Console.Error.WriteLine(string.Format("{0}: {1}", arg, exception.Message));
            }

        }

        return -1;
    }
}