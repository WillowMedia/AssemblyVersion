
Build package and push to (own) nuget repo

```
KEY=97894cf7-695c-4158-a718-e0ce52edf8e5
VERSION=1.0.5
dotnet restore && dotnet build -c release && dotnet pack -c release  
dotnet nuget push -s https://nuget.willow-media.nl/v3/index.json -k ${KEY} --skip-duplicate nupkg/*.nupkg
```
